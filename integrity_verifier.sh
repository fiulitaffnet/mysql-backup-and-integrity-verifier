#!/usr/bin/env bash

# This script restores a MySQL backup and verify its integrity 
# comparing the calculated checksums before the backup 
# with the ones calculated after the restore.


# Configuration variables

# We will create our restored schemas and tables on this host
restore_host="127.0.0.1"

# Backup path. This is where out tar.gz and original checksums file should be.
backup_path="/root/backup/backup/"

# Skip this dbs while restoring
skip_dbs=("information_schema" "performance_schema" "sys" "mysql")

# This allows us to use the same server from which the dump was made, 
# since we create new schemas with a different name, preserving the original schemas.
restored_db_prefix="restored_"

# Get cli options: -f backup name and -c checksums file
while getopts f:c: flag
do
    case "${flag}" in
	f) backup_name=${OPTARG};;
	c) original_checksums_file=${OPTARG};;
    esac
done

# Check for empty options
if [ "$backup_name" == "" ] || [ "$original_checksums_file" == "" ]; then
	echo -e "Usage: `basename "$0"` -f [BACKUP_NAME] -c [ORIGINAL_CHECKSUMS_FILE]\n"
	echo "Files must exists in this configured path: $backup_path"
	exit 1
fi;

# Check for backup file existance
if test -f "$backup_path$backup_name"; then
	echo "$backup_path$backup_name exists."
else
	echo "$backup_path$backup_name does not exist."
	exit 1
fi

# Check for checksums file existance
if test -f "$backup_path$original_checksums_file"; then
	echo "$backup_path$original_checksums_file exists."
else
	echo "$backup_path$original_checksums_file does not exist."
	exit 1
fi

# Get expanded folder name from backup name, removing trailing .tar.gz and prepending alldb_
folder_name=${backup_name/.tar.gz/}
folder_name=${folder_name##*alldb_}

# Exploding backup...
echo "Exploding $backup_path$backup_name..."
cd $backup_path && tar xvzf $backup_name
echo "Done."

# Get schemas from folder tree. 
schemas=$(cd $backup_path$folder_name && ls -larth | awk '{ print $9 }' | sed "s/^db_//g")

# Creating schemas...
echo "Creating schemas..."
while read -r line; do if [ "$line" != "" ] && [ "$line" != "." ] && [ "$line" != ".." ] && [[ ! " ${skip_dbs[*]} " =~ " ${line} " ]]; then
	mysql -h $restore_host -e "CREATE SCHEMA $restored_db_prefix$line DEFAULT CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_general_ci"
       fi;
done < <(printf '%s\n' "$schemas")

folders=$(cd $backup_path$folder_name && ls -larth | awk '{ print $9 }')

# Stripping definers...
# This could be useful when the definer is missing on the target host.
echo "Stripping definers..."
while read -r line; do if [ "$line" != "" ] && [ "$line" != "." ] && [ "$line" != ".." ] && [[ ! " ${skip_dbs[*]} " =~ " db_${line} " ]]; then
       files=$(cd $backup_path$folder_name/$line && ls -larth | awk '{ print $9 }')

	while read -r file; 
		do if [[ $file == *.sql ]]; then
			echo "$backup_path$folder_name/$line/$file"
			sed -i -E 's/DEFINER=.+`\*/*/g' $backup_path$folder_name/$line/$file
			sed -i -E 's/DEFINER=(.+`)[ ]//g' $backup_path$folder_name/$line/$file
		fi;
	done < <(printf '%s\n' "$files") 

	fi;
done < <(printf '%s\n' "$folders")

# Creating and populating tables...
echo "Creating and populating tables..."
while read -r line; do if [ "$line" != "" ] && [ "$line" != "." ] && [ "$line" != ".." ] && [[ ! " ${skip_dbs[*]} " =~ " db_${line} " ]]; then
       files=$(cd $backup_path$folder_name/$line && ls -larth | awk '{ print $9 }')

	while read -r file; 
		do if [[ $file == *.sql ]]; then
			db="${line/db_/}"
			mysql -h $restore_host --database $restored_db_prefix$db < $backup_path$folder_name/$line/$file
		fi;
	done < <(printf '%s\n' "$files") 

	fi;
done < <(printf '%s\n' "$folders")

# Calculating new checksums...
echo "Calculating new checksums..."
while read -r line; do if [ "$line" != "" ] && [ "$line" != "." ] && [ "$line" != ".." ] && [[ ! " ${skip_dbs[*]} " =~ " db_${line} " ]]; then
       files=$(cd $backup_path$folder_name/$line && ls -larth | awk '{ print $9 }')

	while read -r file; 
		do if [[ $file == *.sql ]]; then
			db="${line/db_/}"
			table="${file/.sql/}"
			mysql -N -h $restore_host -e "CHECKSUM TABLE $restored_db_prefix$db.$table" 
		fi;
		done < <(printf '%s\n' "$files") 

	fi;
done < <(printf '%s\n' "$folders") > $backup_path/checksums_restored.txt


# Verify checksums...
echo "Verifying checksums..."
total_tables=0
while read -r line_original; 
	do 
	total_tables=$(($total_tables+1))
	current_db=$(echo $line_original | awk '{ print $1 }')
	current_table=$(echo $line_original | awk '{ print $2 }')
	current_checksum=$(echo $line_original | awk '{ print $4 }')
	echo "Now checking: $current_db.$current_table"
	while read -r line_restored;
		do
			if [[ $(echo $line_restored | awk '{ print $1 }') == $restored_db_prefix$current_db.$current_table ]]; then
				current_restored_checksum=$(echo $line_restored | awk '{ print $2 }')
				if [[ $current_checksum == $current_restored_checksum ]]; then
					echo -e "-- CHECKSUMS MATCH: $current_checksum $current_restored_checksum\n"
				else
					echo -e "-- CHECKSUMS DOES NOT MATCH: $current_checksum $current_restored_checksum\n"
				fi
				break
			fi 

		done < <(cat "$backup_path/checksums_restored.txt")
	
done < <(cat "$backup_path$original_checksums_file") > $backup_path/checksums_verified.txt

grep_no_match=$(grep "CHECKSUMS DOES NOT MATCH" $backup_path/checksums_verified.txt)
if [[ $grep_no_match != "" ]]; then
	echo "One or more checksums didn't match. Please check $backup_path/checksums_verified.txt."
fi

count_ok=$(grep -c "CHECKSUMS MATCH" $backup_path/checksums_verified.txt)
echo "$count_ok out of $total_tables tables are ok."

# Dropping schemas...
echo "Dropping schemas..."
while read -r line; do if [ "$line" != "" ] && [ "$line" != "." ] && [ "$line" != ".." ] && [[ ! " ${skip_dbs[*]} " =~ " ${line} " ]]; then
        mysql -h $restore_host -e "DROP SCHEMA $restored_db_prefix$line"
        fi;
done < <(printf '%s\n' "$schemas")

# Deleting extracted folder...
echo "Deleting extracted folder..."
rm -r $backup_path$folder_name

# Done.
echo "Done, exiting."
