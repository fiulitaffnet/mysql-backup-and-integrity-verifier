# -*- coding: utf-8 -*-
import os
import sys
from datetime import datetime
import MySQLdb

host            = "127.0.0.1"
backup_path     = "/root/backup/backup/" # With trailing /
debug           = True

def debug(message):
    if debug:
        print(message)

def backup( host, 
            backup_path, 
            skip_dbs=['sys','information_schema','performance_schema','mysql'], 
            skip_tables=[] ):

    debug("Connecting to "+host+"...")

    try:
        db = MySQLdb.connect(host=host,read_default_file='~/.my.cnf')
    except Exception as e:
        print("Can't conenct to mysql server: "+str(e))
        sys.exit(1)
    
    debug("OK.")

    date_and_hour = datetime.now().strftime("%Y_%m_%d_%H-%M-%S")
    status_before_dump_file = backup_path + "status_before_dump_"+date_and_hour+"_"+host+".txt"

    debug("Status file is here: "+status_before_dump_file)

    # Cursors
    cdatabase = db.cursor()
    ctable = db.cursor()
    ccurs = db.cursor()

    cdatabase.execute("""SHOW DATABASES""")
    current_db = cdatabase.fetchone()

    try:
        os.makedirs(backup_path + date_and_hour)
    except:
        print("Can't create backup paths.")
        sys.exit(1)

    try:
        status_file = open(status_before_dump_file, "w")
    except:
        print("Can't open "+status_before_dump_file+" for writing.")
        sys.exit(1)

    # Iterate databases
    while current_db is not None:
        
        db = current_db[0]
        
        if db in skip_dbs:
            current_db = cdatabase.fetchone()
            continue

        debug("Current db is: "+db)

        try:
            os.makedirs(backup_path + date_and_hour + "/db_" + db)
        except:
            print("Can't create backup path for db "+db+".")
            sys.exit(1)
        
        
        ctable.execute("""USE """ + db)
        ctable.execute("""SHOW TABLES""")
        current_table = ctable.fetchone()
        
        # Iterate tables
        while current_table is not None:
            
            table = current_table[0]
            
            if table in skip_tables:
                current_table = ctable.fetchone()
                continue
            
            debug("-- Current table is: "+table)

            # Lock table
            #ccurs.execute("""LOCK TABLES """ + table + """ READ""")

            # Check table
            check_table_result = ccurs.execute("""CHECK TABLE """+table)
            check_table = ccurs.fetchone()
            
            if check_table[3]=='OK':

                # Get table checksum
                checksum_result = ccurs.execute("""CHECKSUM TABLE """+table)
                checksum = ccurs.fetchone()

                debug("-- CHECKSUM is: "+str(checksum[1]))

                # Write checksum to file
                status_file.write(db + "\t" + table + "\tOK\t" + str(checksum[1]) + "\n")

                debug("-- Dumping table...")

                # Executing table dump
                exit_status = os.system("/usr/bin/mysqldump --default-character-set=utf8mb4 --single-transaction=true --lock-tables=false --routines --max_allowed_packet=1024M -h " + host + " " + db + " " + table + " > " + backup_path + date_and_hour + "/db_" + db + "/" + table + ".sql")
                
                if exit_status != 0:
                    print("Error dumping table " + str(db) + "." + str(table))
                else:
                    debug("-- Dumped.")

            else:
                debug("-- CHECK TABLE is NOT ok. Skipping.")
                status_file.write(db + "\t" + table + "\tKO\t0\n")

            # Unlocking table
            #ccurs.execute("""UNLOCK TABLES""")        

            # Next table
            current_table = ctable.fetchone()
        
        # Next db
        current_db = cdatabase.fetchone()

    cdatabase=None
    ctable=None
    ccurs=None
    db=None
    status_file.close()

    debug("Compressing folder...")
    compress = os.system("cd " + backup_path + " && tar cf - " + date_and_hour + " | gzip -9 - > " + backup_path + "alldb_" + date_and_hour + ".tar.gz")

    if compress != 0:
        print("Can't compress backup folder.")
        sys.exit(1)
    
    debug("OK.")
    debug("Compressed folder file is: " + backup_path + "alldb_" + date_and_hour + ".tar.gz")

    delete_folder = os.system("cd " + backup_path + " && rm -rf " + date_and_hour)

    if delete_folder != 0:
        print("Can't delete backup folder after compression.")
        sys.exit(1)

    print("Done; exiting.")

if __name__ == "__main__":

    backup(host,backup_path)
